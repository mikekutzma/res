from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, PasswordField, BooleanField, TextAreaField
from wtforms.validators import DataRequired, URL, Email, EqualTo, ValidationError, Length
from app.models import User

class ArticleSubmissionForm(FlaskForm):
    title = StringField('Title',validators=[DataRequired()])
    url = StringField('URL',validators=[DataRequired(),URL()])
    submit = SubmitField('Submit')

class LoginForm(FlaskForm):
    username = StringField('Username',validators=[DataRequired()])
    password = PasswordField('Password',validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')

class RegistrationForm(FlaskForm):
	username = StringField('Username',validators=[DataRequired()])
	email = StringField('Email',validators=[DataRequired(),Email()])
	password = PasswordField('Password',validators=[DataRequired()])
	password2 = PasswordField('Repeat Password',validators=[DataRequired(),EqualTo('password')])
	submit = SubmitField('Register')

	def validate_username(self,username):
		user = User.query.filter_by(username=username.data).first()
		if user is not None:
			raise ValidationError('Please use a different username.')

	def validate_email(self,email):
		user = User.query.filter_by(email=email.data).first()
		if user is not None:
			raise ValidationError('Please use a different email.')

class PostSubmissionForm(FlaskForm):
    title = StringField('Title',validators=[DataRequired()])
    body = TextAreaField('Body',validators=[DataRequired(),Length(min=10)])
    submit = SubmitField('Post')