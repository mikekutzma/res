from app import app, t, db
from flask import render_template, request, redirect, escape, url_for, flash
from app.forms import ArticleSubmissionForm, LoginForm, RegistrationForm, PostSubmissionForm
from app.models import User, Article, Post
from article import get_thumbnail
from flask_login import current_user, login_user, logout_user, login_required
from werkzeug.urls import url_parse

@app.route('/')
@app.route('/home')
def home():
    return render_template('home.html')

@app.route('/feed')
def feed_page():
    return render_template('feed.html',tweets = t.tweets())

@app.route('/news')
def news_index():
    articles = Article.query.order_by(Article.timestamp).all()
    return render_template('news.html',articles = articles)

@app.route('/news/submit',methods=['GET','POST'])
def submit_article():
    form = ArticleSubmissionForm()
    if form.validate_on_submit():
        thumbnail = get_thumbnail(form.url.data)
        article = Article(title=form.title.data,url=form.url.data,thumbnail=thumbnail)
        db.session.add(article)
        db.session.commit()
        return redirect(url_for('news_index'))
    return render_template('submitarticle.html',form=form)

@app.route('/login',methods=['GET','POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if (user is None) or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user,remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('home')
        return redirect(next_page)
    return render_template('login.html',form=form)

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('home'))

@app.route('/register',methods=['GET','POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data,email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Registration successful.')
        return redirect(url_for('login'))
    return render_template('register.html',form=form)

@app.route('/learn')
def learn_page():
    return render_template('learn.html')

@app.route('/contact')
def contact_page():
    return render_template('contact.html')

@app.route('/media')
def media_page():
    return render_template('media.html')

@app.route('/blog')
def blog_index():
    posts = Post.query.order_by(Post.timestamp).all()
    users = [User.query.filter_by(id=post.user_id).first() for post in posts]
    return render_template('blog_index.html',posts_users=zip(posts,users))

@app.route('/blog/submit',methods=['GET','POST'])
@login_required
def submit_post():
    form = PostSubmissionForm()
    if form.validate_on_submit():
        user_id = current_user.id
        post = Post(title=form.title.data,body=form.body.data,user_id=user_id)
        db.session.add(post)
        db.session.commit()
        return redirect(url_for('blog_index'))
    return render_template('submitpost.html',form=form)

@app.route('/blog/posts/<post_id>')
def blog_post(post_id):
    post = Post.query.filter_by(id=post_id).first()
    if post is None:
        return redirect(url_for('blog_index'))
    user = User.query.filter_by(id=post.user_id).first()
    formatted_body = "<br>".join(post.body.split("\n"))
    return render_template('postview.html',post=post,formatted_body=formatted_body,user=user)