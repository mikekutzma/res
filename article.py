from bs4 import BeautifulSoup
import requests

def get_thumbnail(url):
    def_url = "https://res.cloudinary.com/teepublic/image/private/s--mQKP6Dsa--/t_Preview/b_rgb:262c3a,c_limit,f_jpg,h_630,q_90,w_630/v1501205125/production/designs/1770518_1.jpg"
    try:
        data = requests.get(url).text
        soup = BeautifulSoup(data,"html.parser")
        x = soup.find(property="og:image")
        if x:
            return x['content']
        else:
            return def_url
    except:
        return def_url
