# Round Earth Shills
This is the repository for the _Round Earth Shills_ website. The purpose of this site is to ~~piss Frank off~~ spread the truth about the round earth conspiracy.

Because this is a private repository, the fact that you are reading this means you have been given access.

## Getting Started

### Git
In order to pull and push to this repository, you will need to have `git` installed on your system. You can download git for your operating system [here](https://git-scm.com/downloads).

Some thorough git tutorials can be found [here](https://www.atlassian.com/git/tutorials), however, a basic overview of git commands follows:

```
git clone https://mikekutzma@bitbucket.org/mikekutzma/res.git
```
will create a local copy of this repository on your machine in a folder called `res` in whatever directory you run the command from.
```
git status
```
will give the status of the local repository. Here you can see which branch you are currently on, as well as the status of your commits and tracked/untracked files.
```
git branch
```
will list all branches, showing an asterisk next to the branch you are currently on.
```
git checkout some-branch-name
```
will move you into the branch with name `some-branch-name`, _if_ that branch exists. The shortcut for creating and switching to a new branch is to pass the `-b` flag to the `git checkout` command. Therefore, `git checkout -b my-new-branch` will create a new branch titled `my-new-branch` and make this your current branch. Initially, a new branch will be identical to the most recent commit in whichever branch the user was in when the branch was created.
```
git add file1 file2
```
will stage the files `file1 file 2` to be committed. Essentially, this is saying that you want to include the changes made to these files to your next commit. Most often, this command is ran as 
```
git add .
```
which adds any files that have been changed. The `git add` command if usually ran right before the
```
git commit -m 'fix this bug'
```
which commits the changes queued from any add commands to your local repository. This will make these changes with a message of 'fix this bug'. Commit messages are important for other developers (including yourself) to know what what done in a particular commit.
```
git push -u origin some-branch
```
will push any new commits from your local repository to the remote repository `origin`, which is the main remote repo, on the branch `some-branch`. This is what will allow any contributors to pull your changes down into their local repositories.

A final command is the `git pull` command, which will pull any changes made to the remote repo to your local machine. 

So you've cloned the repo, created a new branch for some bug or feature, made some changes, committed your changes to your local (with some useful commit messages) and pushed the changes to this branch to the remote. All that is left is to submit a _pull request_ for your changes to be integrated into the main branch.

More info on pull requests can be found [here](https://www.atlassian.com/git/tutorials/making-a-pull-request).

### Python & Flask

While I won't be explaining any of the code used in this project, here I will give the information necessary to run the app locally, allowing for testing.

* First step is to head over to the [Python Downloads page](https://www.python.org/downloads/release/python-364/) and download the Python installer.
* During installation, the default options should be fine, but be sure to choose the options to install `pip` and to add python to your PATH. This will make life easier for you.
* After installation, open a terminal and type `python --version` and ensure that the version is newer than or equal to 3.6.4
* With python installed, we will now create a virtual environment for you to work in. This will help keep our project isolated from any work done on your computer. When in the `res` directory, type `python -m venv venv`. This will create a virtual environment called `venv` in the working directory. This directory will not be tracked by git in our repo, as there may be subtle differences in the directory structure for different operating systems.
* Now that the virtual environment is created, enter the environment by typing `source venv/bin/activate` on \*NIX operating systems and `venv/Scripts/activate` on Windows from within the `res` folder. You should now see a `(venv)` next to your command prompt. Inside here, you have a fresh install of python with no packages and no environment variables.
* We now need to install all python packages used by our app. The dependencies are collected in the `requirements.txt` file. In order to install necessary packages, run `pip install -r requirements.txt` from inside your local `res` folder while the virtual environment is activated.
* If all packages install sucessfully, you can not set the environment variables. Do this by typing `export FLASK_APP=res.py`. For now, this is the only necessary environment vairable that needs to be set, however, the `FLASK_DEBUG=1` variable is also useful to have set during development.
* With the virtual environment all set up, you should be able to run `flask run` to start the server locally.

This will host the app locally, if the default settings are not changed, at `127.0.0.1:5000` but the console in your terminal should give this information as well. Open up a browser and point it at this address and hopefully you should see our site. 